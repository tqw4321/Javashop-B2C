@echo off
cd /d %~dp0
echo %cd%
set GRADLE_OPTS=-Dspring.profiles.active="development" -Xmx512m -XX:MaxPermSize=512m -Xdebug -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=n
gradle  jettyRun & pause
