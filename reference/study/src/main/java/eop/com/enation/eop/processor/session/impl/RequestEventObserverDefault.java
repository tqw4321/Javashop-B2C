package com.enation.eop.processor.session.impl;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.log4j.Logger;

import com.enation.eop.processor.session.RequestEventObserver;

public class RequestEventObserverDefault implements RequestEventObserver {

	protected final Logger logger = Logger.getLogger(getClass());
	
	@Override
	public void completed(ServletRequest servletRequest, ServletResponse response) {
		logger.debug("request is completed!");
		
	}

}
