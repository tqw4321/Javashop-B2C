package com.enation.framework.gzip;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class GZIPFilter implements Filter {

	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
    public void doFilter(ServletRequest req, ServletResponse res,FilterChain chain) throws IOException, ServletException {
    	logger.debug("------------------doFilter---------------");
    	//
    	if (req instanceof HttpServletRequest) {
    		HttpServletRequest request = (HttpServletRequest) req;
    		HttpServletResponse response = (HttpServletResponse) res;
    		String ae = request.getHeader("accept-encoding");
    		
    		//判断浏览器支持的编码类型，具体通过调试可以看出来
    		if (ae != null && ae.indexOf("gzip") != -1) {
    			
	            GZIPResponseWrapper gZIPResponseWrapper = new GZIPResponseWrapper(response);
	            chain.doFilter(req, gZIPResponseWrapper);
	            gZIPResponseWrapper.finishResponse();
	            return;
    		}
        chain.doFilter(req, res);            
   }else{
   }
}

    public void init(FilterConfig filterConfig) {
    	
    	logger.debug("------------------GZIPFilter---------------");
    	
    }

    public void destroy() {
        //
    }
}
