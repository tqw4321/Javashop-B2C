package com.enation.eop.processor.facade;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

import com.enation.eop.IEopProcessor;
import com.enation.eop.processor.core.HttpHeaderConstants;
import com.enation.eop.sdk.context.EopContextIniter;
import com.enation.framework.context.webcontext.ThreadContextHolder;
/**
 * web资源处理器，响应js,css,图片和flash
 * @author kingapex
 *2010-2-27上午12:11:26
 */
public class ResourceProcessor implements IEopProcessor {

	
	public boolean process() throws IOException {
		//这里之所有从ThreadContextHolder获取 response，httpRequest
		//是因为在DispatcherFilter 中的下面这段代码中，将httpRequest,httpResponse保存到了
		//ThreadContextHolder中了，目的就是便于在多处获取这个变量
		//EopContextIniter.init(httpRequest, httpResponse, requestEventSubject);
		
		 HttpServletResponse response = ThreadContextHolder.getHttpResponse();
		 HttpServletRequest httpRequest = ThreadContextHolder.getHttpRequest();
		 				
		 String path  = httpRequest.getServletPath();
		 
		 //不是以/resource/头的资源请求,全部返回false
		 //在b2c/src/main/webapp下目前没有/resource/目录，所以都是返回flase
		 //且目前的前端模板实际上是:themes/kaben
		 //后台管理的模板是:adminthemes
		 //所以下面这个判断都是以return false;回去。
		 //因此不会再走下面的代码。
		 if (!path.startsWith("/resource/")) {
			 return false;//返回false,就由上图的判断直接输出了，不再走下面的url判断
		 }
		 //因此下面的代码，在目前的版本中基本不执行
		 //只有当页面需要请求/resource/目录下的静态资源时才执行。
		 //二次开发时，如果有自己的一些js,css等资源，可以放到这个目录下
		 //不过从下面的IOUtils.write(inbytes, output);来看，是没有压缩输出的
		 path = path.replaceAll("/resource/","");
	 
			if(path.toLowerCase().endsWith(".js")) response.setContentType(HttpHeaderConstants.CONTEXT_TYPE_JAVASCRIPT);
			if(path.toLowerCase().endsWith(".css")) response.setContentType(HttpHeaderConstants.CONTEXT_TYPE_CSS);
			if(path.toLowerCase().endsWith(".jpg")) response.setContentType(HttpHeaderConstants.CONTEXT_TYPE_JPG);
			if(path.toLowerCase().endsWith(".gif")) response.setContentType(HttpHeaderConstants.CONTEXT_TYPE_GIF);
			if(path.toLowerCase().endsWith(".png")) response.setContentType(HttpHeaderConstants.CONTEXT_TYPE_PNG);
			if(path.toLowerCase().endsWith(".swf")) response.setContentType(HttpHeaderConstants.CONTEXT_TYPE_FLASH);
			
			InputStream in =   getClass().getClassLoader().getResourceAsStream(path);
			if (in != null) {
				byte[] inbytes = IOUtils.toByteArray(in);
			//	response.setCharacterEncoding("UTF-8");

				OutputStream output = response.getOutputStream();
				IOUtils.write(inbytes, output);
				return true;
			} else {
				 return false;
			}
		 
		
	}

}
