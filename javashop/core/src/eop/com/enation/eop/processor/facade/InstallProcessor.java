package com.enation.eop.processor.facade;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enation.eop.IEopProcessor;
import com.enation.eop.sdk.context.EopSetting;
import com.enation.framework.context.webcontext.ThreadContextHolder;
/**
 * 安装处理器，响应/install来的响应
 * @author kingapex
 *2015-3-13
 */
public class InstallProcessor implements IEopProcessor {

	@Override
	public boolean process() throws IOException {
		
		HttpServletResponse httpResponse = ThreadContextHolder.getHttpResponse();
		HttpServletRequest httpRequest = ThreadContextHolder.getHttpRequest();
		String uri = httpRequest.getServletPath();
		
		if (!uri.startsWith("/install")	&& EopSetting.INSTALL_LOCK.toUpperCase().equals("NO")) {
			//第一次启动服务器时，不是以/install开头的uri,但是系统的install/install.lock文件没有生成
			//所以这里修改了uri变成了以/install开头的，然后重定向发起请求。
			//然后再经过DispatcherFilter,再到进入到此类的process方法
			//进入到下面的 if (uri.startsWith("/install")) 并返回false			
			httpResponse.sendRedirect(httpRequest.getContextPath() + "/install");
			return true;
		}
		
		if (uri.startsWith("/install")) {
			if( EopSetting.INSTALL_LOCK.toUpperCase().equals("NO")){
			//这里返回false后，由chain分发，会匹配到 /install/index.html
			//在install/index.html中有如下语句,再发起请求进入了/install/setp1.do
			//	<title>Javashop安装程序</title>
			//	<script>location.href='step1.do'</script>
			//这样就匹配到了 EopInstallController 这个控制器,进入接下来的安装处理
				return false; //要由chain处理
			}else{
				return true; //拒绝再执行
			}
			 
		}
		
		return true;
	}

	
}
