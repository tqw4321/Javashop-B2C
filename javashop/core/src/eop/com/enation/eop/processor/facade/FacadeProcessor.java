package com.enation.eop.processor.facade;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enation.app.base.core.model.ClusterSetting;
import com.enation.eop.IEopProcessor;
import com.enation.eop.SystemSetting;
import com.enation.framework.context.webcontext.ThreadContextHolder;
import com.enation.framework.util.RequestUtil;

/**
 * 前台模板处理器<br>
 * docs目录 由文档解析器处理
 * 其它目录由模板处理器处理
 * @author kingapex
 *2015-3-13
 */
public class FacadeProcessor implements IEopProcessor{

	public boolean process()throws IOException, ServletException{
		HttpServletRequest httpRequest =ThreadContextHolder.getHttpRequest();
		HttpServletResponse httpResponse=ThreadContextHolder.getHttpResponse();

		String uri =httpRequest.getServletPath();
		ThreadContextHolder.getHttpResponse().setContentType("text/html;charset=UTF-8");
		SsoProcessor processor = new SsoProcessor();
		processor.parse(); 
		
		//帮助文件uri:比如http:http://localhost:8080/b2c/docs/
		//就会显示接口相关文档页面，具体对应于webapp/docs/目录
		if(uri.startsWith("/docs")){
			DocsPageParser docsPageParser = new DocsPageParser();
			docsPageParser.parse(uri);
			return true;
		}

		/**
		 * 如果集群静态页开启的时候  优先选择集群组件的静态页
		 * 否则，再判断静态页组件是否开启
		 * 
		 * add by tqw
		 * 如果后台开启了缓存，那么就首先看看有没有在缓存当前请求的页面
		 * MemoryPageParser 实际是从缓存工厂中获取，缓存的架构在
		 * com.enation.framework.cache 包下面。
		 * 根据对ICache接口的实现，以及数据库中对缓存的方案的设定值，可以有多种缓存方式
		 * 比如redis,memcached,具体看CacheFactory.getCacheFa(),其默认的缓存
		 * 实现是EhCacheImpl.java 在spring_cfg下有一个ehcache.xml具体配置了这个中间
		 * 件的使用参数.
		 * 
		 * memoryPageParser.parse(uri)判别了是否存储了当前页面的缓存，如果有缓存过
		 * 那么就直接输出了。如果没有缓存，那么就经FacadePageParser页面处理完成。
		 * 页面内容缓存到缓存器的流程需要另外研究一下：其涉及到两个方面
		 * 1：页面是如何缓存的,缓存的时刻点
		 * 2：网站后台内容更新后，及时更新缓存的页面。
		 */
		if(ClusterSetting.getStatic_page_open() == 1){
			//集群缓存，首先要有架设redis,memcache类似的缓存中间件
			MemoryPageParser memoryPageParser = new MemoryPageParser();
			boolean result =memoryPageParser.parse(uri);
			if(!result){
				FacadePageParser parser = new FacadePageParser();
				return parser.parse(uri);
			}else{
				return true;
			}
		}else{
			if(SystemSetting.getStatic_page_open()==1){
				StaticPageParser staticPageParser = new StaticPageParser();
				boolean result =staticPageParser.parse(uri);
				if(!result){
					FacadePageParser parser = new FacadePageParser();
					return parser.parse(uri);
				}else{
					return true;
				}
			}else{
				FacadePageParser parser = new FacadePageParser();
				return parser.parse(uri);
			}
		}

	}

}
