package com.enation.app.cinema;

import org.springframework.stereotype.Service;

import com.enation.eop.resource.model.EopSite;
import com.enation.eop.sdk.App;

@Service("cinema")
public class CinemaApp extends App {

	@Override
	public void install() {
		this.doInstall("file:com/enation/app/cinema/cinema.xml");	
	}

	@Override
	public void sessionDestroyed(String sessionid, EopSite site) {	
	}

	@Override
	public String getName() {
		return "cinema应用";
	}

	@Override
	public String getId() {
		return "cinema";
	}

	@Override
	public String getNameSpace() {
		return "/cinema";
	}

}
