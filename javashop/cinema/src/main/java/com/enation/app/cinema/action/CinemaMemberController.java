package com.enation.app.cinema.action;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.enation.app.base.core.model.Member;
import com.enation.app.base.core.service.IMemberManager;
import com.enation.app.base.core.service.ISmsManager;
import com.enation.app.base.core.util.SmsTypeKeyEnum;
import com.enation.app.base.core.util.SmsUtil;
//import com.enation.app.shop.core.member.service.IMemberPointManger;
import com.enation.eop.sdk.utils.ValidCodeServlet;
import com.enation.framework.action.JsonResult;
import com.enation.framework.context.webcontext.ThreadContextHolder;
import com.enation.framework.jms.EmailProducer;
import com.enation.framework.util.EncryptionUtil1;
import com.enation.framework.util.HttpUtil;
import com.enation.framework.util.JsonResultUtil;
import com.enation.framework.util.StringUtil;

@Controller
@Scope("prototype")
@RequestMapping(value="/cinema/api/member")
public class CinemaMemberController {
	private static final Logger LOGGER = LoggerFactory.getLogger(CinemaMemberController.class);
	
	@Autowired
	private IMemberManager memberManager;
	@Autowired	
	private EmailProducer mailMessageProducer;
//	@Autowired
//	private IMemberPointManger memberPointManger;
	@Autowired
	private ISmsManager smsManager;
	/**
	 * 会员登录
	 * @param username:用户名,String型
	 * @param password:密码,String型
	 * @param validcode:验证码,String型
	 * @param remember:两周内免登录,String型
	 * @return json字串
	 * result  为1表示登录成功，0表示失败 ，int型
	 * message 为提示信息 ，String型
	 */
	@ResponseBody
	@RequestMapping(value="/login",produces = MediaType.APPLICATION_JSON_VALUE)
	public JsonResult login(String validcode, String username, String password, String remember,Integer type,String mobile) {
		/**
		 * 由江宏岩修改
		 * 为支持手机号发送验证码登录
		 */
		if(type==null||type!=0){
			if (this.validcode(validcode,"memberlogin") == 1) {
				int result = this.memberManager.login(username, password);
				if (result == 1) {

					remember = "1"; //add_by Sylow 默认支持两周免登录

					// 两周内免登录
					if (remember != null && remember.equals("1")) {
						String cookieValue = EncryptionUtil1.authcode(
								"{username:\"" + username + "\",password:\"" + StringUtil.md5(password) + "\"}",
								"ENCODE", "", 0);
						HttpUtil.addCookie(ThreadContextHolder.getHttpResponse(), "JavaShopUser", cookieValue, 60 * 24 * 14);
					}
					shiroLogin(username, password);
					return JsonResultUtil.getSuccessJson("登录成功");
				}else{
					return JsonResultUtil.getErrorJson("账号密码错误");				
				}
			} 
			return JsonResultUtil.getErrorJson("验证码错误！");	
		}else{
			try {
				if (SmsUtil.validSmsCode(validcode, mobile, SmsTypeKeyEnum.LOGIN.toString())) {
					Member member = this.memberManager.loginByMobile(mobile);
					if (member!=null) {
						
						remember = "1"; //add_by Sylow 默认支持两周免登录
						
						// 两周内免登录
						if (remember != null && remember.equals("1")) {
							String cookieValue = EncryptionUtil1.authcode(
									"{username:\"" + member.getUname() + "\",password:\"" + StringUtil.md5(member.getPassword()) + "\"}",
									"ENCODE", "", 0);
							HttpUtil.addCookie(ThreadContextHolder.getHttpResponse(), "JavaShopUser", cookieValue, 60 * 24 * 14);
						}
						//手机登录时没有密码
						shiroLogin(member.getUname(), validcode);
						return JsonResultUtil.getSuccessJson("登录成功");
					}else{
						return JsonResultUtil.getErrorJson("账号密码错误");				
					}
				} 
			} catch (Exception e) {
				return JsonResultUtil.getErrorJson(e.getMessage());	
			}
			return JsonResultUtil.getErrorJson("验证码错误！");	
		}
	}
	
	
	/**
	 * 通过Shiro登录，Shiro权限过滤器才能生效。
	 * @param username 用户名
	 * @param password 密码
	 */
	private void shiroLogin(String username, String password) {
		try {
			UsernamePasswordToken token = new UsernamePasswordToken(username, password);
			Subject subject = SecurityUtils.getSubject();
			subject.login(token);
		} catch (AuthenticationException e) {
			LOGGER.error("Shiro subject login failed", e);
		}
	}
	
	/**
	 * 校验验证码
	 * 
	 * @param validcode
	 * @param name (1、memberlogin:会员登录  2、memberreg:会员注册 3、membervalid:会员手机验证)
	 * @return 1成功 0失败
	 */
	private int validcode(String validcode,String name) {
		if (validcode == null) {
			return 0;
		}

		String code = (String) ThreadContextHolder.getSession().getAttribute(ValidCodeServlet.SESSION_VALID_CODE + name);

		if (code == null) {
			return 0;
		} else {
			if (!code.equalsIgnoreCase(validcode)) {
				return 0;
			}
		}
		return 1;
	}
}
